use std::time::Instant;

pub fn exec() {
    let raw_input: String = file_input::read_to_string("inputs/day01/problem.txt");
    println!("Advent of Code: 2021-12-01");
    let start = Instant::now();
    let input = parse_input(&raw_input);
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &[i16]) -> i16 {
    count_sliding_increases(input, 1)
}

pub fn part_two(input: &[i16]) -> i16 {
    count_sliding_increases(input, 3)
}

pub fn count_sliding_increases(input: &[i16], window: usize) -> i16 {
    let mut increases: i16 = 0;
    for index in window..input.len() {
        if input[index] > input[index - window] {
            increases += 1;
        }
    }
    increases
}

pub fn parse_input(input: &str) -> Vec<i16> {
    let mut values: Vec<i16> = Vec::with_capacity(2000);

    let bytes = input.as_bytes();
    let mut value: i16 = 0;
    for byte in bytes {
        match *byte {
            b'\n' => {
                values.push(value);
                value = 0;
            }
            v => {
                value *= 10;
                value += (v - b'0') as i16;
            }
        }
    }
    values
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day01/problem.txt",    1475),
        part_one_1:         ("inputs/day01/test01.txt",     7),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day01/problem.txt",    1516),
        part_two_1:         ("inputs/day01/test01.txt",     5),
    }
}
