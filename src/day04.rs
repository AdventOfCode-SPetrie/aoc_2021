use std::convert::From;
use std::time::Instant;

pub fn exec() {
    let raw_input: String = file_input::read_to_string("inputs/day04/problem.txt");
    println!("Advent of Code: 2021-12-04");
    let start = Instant::now();
    let input = parse_input(&raw_input);
    println!("P1: {}", part_one(&input.0, &input.1));
    println!("P2: {}", part_two(&input.0, &input.1));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(numbers: &[u8], boards: &[BingoBoard]) -> u64 {
    let mut owned_boards: Vec<BingoBoard> = boards.to_vec();
    for number in numbers {
        for i in 0..owned_boards.len(){
            owned_boards[i].select_number(number);
            if owned_boards[i].won {
                return owned_boards[i].sum_unused() * u64::from(*number);
            }
        }
    }
    //unreachable!()
    0
}

pub fn part_two(numbers: &[u8], boards: &[BingoBoard]) -> u64 {
    let mut owned_boards: Vec<BingoBoard> = boards.to_vec();
    for number in numbers {
        for i in 0..owned_boards.len(){
            owned_boards[i].select_number(number);
            if owned_boards[i].won && owned_boards.len() == 1 {
                return owned_boards[i].sum_unused() * u64::from(*number);
            }
        }
        owned_boards.retain(|board| !board.won);
    }
    //unreachable!()
    0
}

pub fn parse_input(input: &str) -> (Vec<u8>, Vec<BingoBoard>) {
    let mut values: Vec<u8> = Vec::with_capacity(100);
    let mut boards: Vec<BingoBoard> = Vec::with_capacity(100);

    let bytes = input.as_bytes();
    let mut value: u8 = 0;
    let mut index: usize = 0;
    while index < bytes.len() {
        match bytes[index] {
            b'\n' => {
                values.push(value);
                index += 2;
                break;
            }
            b',' => {
                values.push(value);
                value = 0;
            }
            v => {
                value *= 10;
                value += v - b'0';
            }
        }
        index += 1;
    }
    while index < bytes.len() - 75 {
        let end: usize = index + 75;
        boards.push(BingoBoard::from(&bytes[index..end]));
        index += 76;
    }
    (values, boards)
}

#[derive(Copy, Clone)]
pub struct BingoBoard {
    won: bool,
    state: u32,
    mask: u128,
    numbers: [u8; 25],
}

const ROW_0: u32 = 0b00000_00000_00000_00000_11111;
const ROW_1: u32 = 0b00000_00000_00000_11111_00000;
const ROW_2: u32 = 0b00000_00000_11111_00000_00000;
const ROW_3: u32 = 0b00000_11111_00000_00000_00000;
const ROW_4: u32 = 0b11111_00000_00000_00000_00000;
const COL_0: u32 = 0b10000_10000_10000_10000_10000;
const COL_1: u32 = 0b01000_01000_01000_01000_01000;
const COL_2: u32 = 0b00100_00100_00100_00100_00100;
const COL_3: u32 = 0b00010_00010_00010_00010_00010;
const COL_4: u32 = 0b00001_00001_00001_00001_00001;

impl BingoBoard {
    #[inline(always)]
    pub fn update_won(&mut self) {
        match self.state {
            _ if (self.state & ROW_0) == ROW_0 => self.won = true,
            _ if (self.state & ROW_1) == ROW_1 => self.won = true,
            _ if (self.state & ROW_2) == ROW_2 => self.won = true,
            _ if (self.state & ROW_3) == ROW_3 => self.won = true,
            _ if (self.state & ROW_4) == ROW_4 => self.won = true,
            _ if (self.state & COL_0) == COL_0 => self.won = true,
            _ if (self.state & COL_1) == COL_1 => self.won = true,
            _ if (self.state & COL_2) == COL_2 => self.won = true,
            _ if (self.state & COL_3) == COL_3 => self.won = true,
            _ if (self.state & COL_4) == COL_4 => self.won = true,
            _ => {},
        }
    }
    #[inline(always)]
    pub fn select_number(&mut self, number: &u8) {
        let mut updated: bool = false;
        if self.mask & 1u128 << *number  == 0 {
            return;
        }
        for i in 0..25 {
            if self.numbers[i] == *number {
                self.state |= 1 << i;
                updated = true;
            }
        }
        if updated {
            self.update_won();
        }
    }
    #[inline(always)]
    pub fn sum_unused(&self) -> u64 {
        let mut sum: u64 = 0;
        for i in 0..25 {
            if self.state & (1 << i) == 0 {
                sum += u64::from(self.numbers[i]);
            }
        }
        sum
    }
}

impl From<&[u8]> for BingoBoard {
    fn from(slice: &[u8]) -> Self {
        let mut board: BingoBoard = BingoBoard {
            won: false,
            state: 0,
            mask: 0,
            numbers: [0; 25],
        };
        let mut index: usize = 0;
        let mut number: usize = 0;
        let mut value: u8 = 0;
        while index < slice.len() {
            if slice[index] != b' ' {
                value += slice[index] - b'0';
                value *= 10;
            }
            index += 1;
            value += slice[index] - b'0';
            board.numbers[number] = value;
            board.mask |= 1 << value;
            value = 0;
            number += 1;
            index += 2;
        }
        board
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input.0, &input.1), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected,) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input.0, &input.1), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day04/problem.txt",     63552),
        part_one_1:         ("inputs/day04/test01.txt",      4512),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day04/problem.txt",     9020),
        part_two_1:         ("inputs/day04/test01.txt",      1924),
    }
}
