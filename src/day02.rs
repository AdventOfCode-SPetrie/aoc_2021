use std::time::Instant;

pub fn exec() {
    let raw_input: String = file_input::read_to_string("inputs/day02/problem.txt");
    println!("Advent of Code: 2021-12-02");
    let start = Instant::now();
    let input = parse_input(&raw_input);
    println!("P1: {}", part_one(&input));
    println!("P2: {}", part_two(&input));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &[Instruction]) -> i32 {
    let mut horizontal: i32 = 0;
    let mut depth: i32 = 0;
    for instruction in input {
        match instruction {
            Instruction::Forward(v) => horizontal += *v,
            Instruction::Down(v) => depth += *v,
            Instruction::Up(v) => depth -= *v,
        }
    }
    horizontal * depth
}

pub fn part_two(input: &[Instruction]) -> i32 {
    let mut horizontal: i32 = 0;
    let mut depth: i32 = 0;
    let mut aim: i32 = 0;
    for instruction in input {
        match instruction {
            Instruction::Forward(v) => {
                horizontal += *v;
                depth += aim * *v;
            }
            Instruction::Down(v) => aim += *v,
            Instruction::Up(v) => aim -= *v,
        }
    }
    horizontal * depth
}

#[derive(Debug)]
pub enum Instruction {
    Forward(i32),
    Down(i32),
    Up(i32),
}

pub fn parse_input(input: &str) -> Vec<Instruction> {
    let mut values: Vec<Instruction> = Vec::with_capacity(2000);

    let bytes = input.as_bytes();
    let mut index: usize = 0;
    while index < bytes.len() {
        match bytes[index] {
            b'f' => {
                index += 8;
                values.push(Instruction::Forward((bytes[index] - b'0') as i32));
                index += 2;
            }
            b'd' => {
                index += 5;
                values.push(Instruction::Down((bytes[index] - b'0') as i32));
                index += 2;
            }
            b'u' => {
                index += 3;
                values.push(Instruction::Up((bytes[index] - b'0') as i32));
                index += 2;
            }
            _ => break,
        }
    }
    values
}

#[allow(dead_code)]
pub fn unified_solution(input: &str) -> (i32, i32) {
    let bytes = input.as_bytes();
    let mut index: usize = 0;
    let mut depth1: i32 = 0;
    let mut depth2: i32 = 0;
    let mut horizontal: i32 = 0;
    let mut aim: i32 = 0;
    while index < bytes.len() {
        match bytes[index] {
            b'f' => {
                index += 8;
                horizontal += (bytes[index] - b'0') as i32;
                depth2 += ((bytes[index] - b'0') as i32) * aim;
                index += 2;
            }
            b'd' => {
                index += 5;
                depth1 += (bytes[index] - b'0') as i32;
                aim += (bytes[index] - b'0') as i32;
                index += 2;
            }
            b'u' => {
                index += 3;
                depth1 -= (bytes[index] - b'0') as i32;
                aim -= (bytes[index] - b'0') as i32;
                index += 2;
            }
            _ => break,
        }
    }
    (horizontal * depth1, horizontal * depth2)
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day02/problem.txt",    2117664),
        part_one_1:         ("inputs/day02/test01.txt",     150),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day02/problem.txt",    2073416724),
        part_two_1:         ("inputs/day02/test01.txt",     900),
    }
}
