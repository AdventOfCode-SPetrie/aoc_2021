use std::cmp::Ordering;
use std::time::Instant;

pub fn exec() {
    let raw_input: String = file_input::read_to_string("inputs/day03/problem.txt");
    println!("Advent of Code: 2021-12-03");
    let start = Instant::now();
    let input = parse_input(&raw_input);
    println!("P1: {}", part_one(&input, 12));
    println!("P2: {}", part_two(&input, 12));
    println!("Elapsed Time: {}ms\n", start.elapsed().as_millis());
}

pub fn part_one(input: &[u16], size: u16) -> u64 {
    let mut gamma: u16 = 0;
    for i in (0..size).rev() {
        gamma <<= 1;
        match one_bit_frequency(input, i) {
            Ordering::Greater | Ordering::Equal => gamma += 1,
            Ordering::Less => {}
        }
    }
    let epsilon: u16 = gamma ^ (u16::MAX >> (16 - size));
    u64::from(gamma) * u64::from(epsilon)
}

pub fn part_two(input: &[u16], size: u16) -> u64 {
    let mut o2_value: u16 = 0;
    let mut co2_value: u16 = 0;
    let mut o2_list: Vec<u16> = Vec::from(input);
    let mut co2_list: Vec<u16> = Vec::from(input);
    for position in (0..size).rev() {
        match one_bit_frequency(&o2_list, position) {
            Ordering::Greater | Ordering::Equal => {
                o2_list.retain(|&v| bit_is_one(v, position));
            }
            Ordering::Less => {
                o2_list.retain(|&v| !bit_is_one(v, position));
            }
        }
        match one_bit_frequency(&co2_list, position) {
            Ordering::Greater | Ordering::Equal => {
                co2_list.retain(|&v| !bit_is_one(v, position));
            }
            Ordering::Less => {
                co2_list.retain(|&v| bit_is_one(v, position));
            }
        }
        if o2_list.len() == 1 {
            o2_value = o2_list[0];
        }
        if co2_list.len() == 1 {
            co2_value = co2_list[0];
        }
        if o2_list.len() <= 1 && co2_list.len() <= 1 {
            break;
        }
    }
    u64::from(o2_value) * u64::from(co2_value)
}

#[inline(always)]
fn bit_is_one(input: u16, position: u16) -> bool {
    ((input >> position) & 1) == 1
}

#[inline(always)]
fn one_bit_frequency(input: &[u16], position: u16) -> Ordering {
    let mut count: usize = 0;
    for value in input {
        count += ((value >> position) & 1) as usize;
    }
    count.cmp(&(input.len() - count))
}

#[allow(dead_code)]
#[inline(always)]
fn one_bit_frequency_iter<'a>(input: impl Iterator<Item = &'a u16>, position: u16) -> Ordering {
    let mut count: usize = 0;
    let mut amount: usize = 0;
    for value in input {
        count += ((value >> position) & 1) as usize;
        amount += 1;
    }
    count.cmp(&(amount - count))
}

pub fn parse_input(input: &str) -> Vec<u16> {
    let mut values: Vec<u16> = Vec::with_capacity(2000);

    let bytes = input.as_bytes();
    let mut value: u16 = 0;
    for byte in bytes {
        match *byte {
            b'\n' => {
                values.push(value);
                value = 0;
            }
            v => {
                value <<= 1;
                value |= (v - b'0') as u16;
            }
        }
    }
    values
}

#[cfg(test)]
mod tests {
    use super::*;
    macro_rules! part_one_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, size, expected) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_one(&input, size), expected);
            }
        )*
        }
    }

    macro_rules! part_two_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (path, size, expected,) = $value;
                let raw_input: String = file_input::read_to_string(path);
                let input = parse_input(&raw_input);
                assert_eq!(part_two(&input, size), expected);
            }
        )*
        }
    }
    part_one_tests! {
        part_one_problem:   ("inputs/day03/problem.txt", 12,     3242606),
        part_one_1:         ("inputs/day03/test01.txt",   5,     198),
    }

    part_two_tests! {
        part_two_problem:   ("inputs/day03/problem.txt", 12,     4856080),
        part_two_1:         ("inputs/day03/test01.txt",   5,     230),
    }
}
